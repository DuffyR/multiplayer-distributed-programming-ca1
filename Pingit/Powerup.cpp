#include "Powerup.hpp"
#include "DataTables.hpp"
#include "Paddle.hpp"
#include "Utility.hpp"
#include "ResourceHolder.hpp"

#include <SFML/Graphics/RenderTarget.hpp>

namespace {
  const TextureData Texture = initializeTextureData()[Data::Pickup];
  const float Speed = 300.f;
  const sf::Time MoveDelay = sf::seconds(3.f);
}

Powerup::Powerup(const TextureHolder & textures, int direction, bool makeSmall)
  : Entity(1)
  , mAnimatedSprite(textures.get(TextureIDs::Powerup))
  , mStartMoveDelay(MoveDelay)
  , mTargetDirection(direction)
  , mMakePaddleSmall(makeSmall)
{
  mAnimatedSprite.setFrameSize(sf::Vector2i(8, 8));
  mAnimatedSprite.setNumFrames(4);
  mAnimatedSprite.setDuration(sf::seconds(0.5f));
  mAnimatedSprite.setRepeating(true);

  centerOrigin(mAnimatedSprite);
}

unsigned int Powerup::getCategory() const {
  return Category::Pickup;
}

sf::FloatRect Powerup::getBoundingRect() const {
  return getWorldTransform().transformRect(mAnimatedSprite.getGlobalBounds());
}

void Powerup::apply(Paddle & player) {
  player.changePaddleSize(mMakePaddleSmall);
}

void Powerup::drawCurrent(sf::RenderTarget & target, sf::RenderStates states) const {
  target.draw(mAnimatedSprite, states);
}

void Powerup::updateCurrent(sf::Time dt, CommandQueue & commands) {
  if (mStartMoveDelay > sf::Time::Zero) {
    mStartMoveDelay -= dt;

    if (mStartMoveDelay <= sf::Time::Zero) {
      setVelocity(mTargetDirection * Speed, 0.f);
    }
  }

  mAnimatedSprite.update(dt);

  Entity::updateCurrent(dt, commands);
}
