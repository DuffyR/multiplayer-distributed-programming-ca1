#include "ResultsState.hpp"
#include "Utility.hpp"
#include "SoundPlayer.hpp"
#include "MusicPlayer.hpp"
#include "ResourceHolder.hpp"

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>

ResultsState::ResultsState(StateStack & stack, Context context)
  : State(stack, context)
  , mCountdownTime(sf::seconds(0.5f))
  , mPhase(0)
{
  context.music->stop();
}

void ResultsState::draw() {
  sf::RenderWindow& window = *getContext().window;
  window.setView(window.getDefaultView());

  // Create green-ish background
  sf::RectangleShape backgroundShape;
  backgroundShape.setFillColor(sf::Color(155, 188, 15));
  backgroundShape.setSize(window.getView().getSize());
  window.draw(backgroundShape);

  for (sf::Text& text : mTexts)
    window.draw(text);
}

bool ResultsState::update(sf::Time dt) {
  mCountdownTime -= dt;

  if (mCountdownTime <= sf::Time::Zero)
    advance();

  return false;
}

bool ResultsState::handleEvent(const sf::Event & event) {
  return false;
}

void ResultsState::advance() {
  sf::Time newInterval;

  switch (mPhase) {
  case 0:
    addText(std::to_string(*getContext().scoreRef1), 0.3f, 0.4f, 60);
    newInterval = sf::seconds(0.7f);
    playSound(SoundEffectIDs::MenuSelection);
    break;
  case 1:
    addText("-", 0.5f, 0.4f, 60);
    newInterval = sf::seconds(0.7f);
    playSound(SoundEffectIDs::MenuSelection);
    break;
  case 2:
    addText(std::to_string(*getContext().scoreRef2), 0.7f, 0.4f, 60);
    newInterval = sf::seconds(1.2f);
    playSound(SoundEffectIDs::MenuSelection);
    break;
  case 3:
    addText(determineWinner(), 0.5f, 0.7f, 90);
    newInterval = sf::seconds(5.f);
    playSound(SoundEffectIDs::GameWon);
    break;
  case 4:
    requestStateClear();
    requestStackPush(StateIDs::Menu);
    break;
  }

  mCountdownTime = newInterval;
  mPhase++;
}

void ResultsState::addText(const std::string& text, float xPos, float yPos, unsigned int charSize) {
  sf::Font& font = getContext().fonts->get(FontIDs::Main);
  sf::Vector2f windowSize(getContext().window->getSize());

  sf::Text newText(text, font, charSize);
  centerOrigin(newText);
  newText.setPosition(xPos * windowSize.x, yPos * windowSize.y);

  mTexts.push_back(newText);
}

const std::string ResultsState::determineWinner() {
  if (*getContext().scoreRef1 > *getContext().scoreRef2)
    return "P1 WINS";
  else if (*getContext().scoreRef1 < *getContext().scoreRef2)
    return "P2 WINS";
  else
    return "DRAW";
}

void ResultsState::playSound(SoundEffectIDs sound) {
  getContext().sounds->play(sound);
}
