#include "KeyBinding.hpp"

KeyBinding::KeyBinding(int controlPreconfiguration)
  : mKeyMap()
{
  // Set initial key bindings for player 1
  if (controlPreconfiguration == 1) {
    mKeyMap[sf::Keyboard::W] = PlayerAction::MoveUp;
    mKeyMap[sf::Keyboard::S] = PlayerAction::MoveDown;
  } else if (controlPreconfiguration == 2) {
    // Player 2
    mKeyMap[sf::Keyboard::Up] = PlayerAction::MoveUp;
    mKeyMap[sf::Keyboard::Down] = PlayerAction::MoveDown;
  }
}

void KeyBinding::assignKey(PlayerAction action, sf::Keyboard::Key key)
{
  // Remove all keys that already map to action
  for (auto itr = mKeyMap.begin(); itr != mKeyMap.end(); ) {
    if (itr->second == action)
      mKeyMap.erase(itr++);
    else
      ++itr;
  }

  // Insert new binding
  mKeyMap[key] = action;
}

sf::Keyboard::Key KeyBinding::getAssignedKey(PlayerAction action) const
{
  for (auto pair : mKeyMap) {
    if (pair.second == action)
      return pair.first;
  }
  return sf::Keyboard::Unknown;
}

bool KeyBinding::checkAction(sf::Keyboard::Key key, PlayerAction & out) const
{
  auto found = mKeyMap.find(key);
  if (found == mKeyMap.end()) {
    return false;
  } else {
    out = found->second;
    return true;
  }
}

std::vector<PlayerAction> KeyBinding::getRealtimeActions() const
{
  // Return all realtime actions that are currently active.
  std::vector<PlayerAction> actions;

  for(auto pair : mKeyMap) {
    // If key is pressed and an action is a realtime action, store it
    if (sf::Keyboard::isKeyPressed(pair.first) && isRealtimeAction(pair.second))
      actions.push_back(pair.second);
  }

  return actions;
}

bool isRealtimeAction(PlayerAction action) {
  switch (action) {
  case PlayerAction::MoveDown:
  case PlayerAction::MoveUp:
    return true;

  default:
    return false;
  }
}
