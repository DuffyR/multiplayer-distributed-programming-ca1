#pragma once
//This class represents the boundaries of the gme world that the ball will bounce off of
#include "Entity.hpp"
#include "Command.hpp"
#include "ResourceIdentifiers.hpp"

#include <SFML/Graphics/Sprite.hpp>

class Wall : public Entity {
public:
  Wall(const TextureHolder& textures, sf::Vector2f size);

  virtual unsigned int	getCategory() const;
  virtual sf::FloatRect	getBoundingRect() const;

  sf::Vector2f getNormal() const;
  void setNormal(sf::Vector2f normal);

private:
  virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
  virtual void updateCurrent(sf::Time dt, CommandQueue& commands);

private:
  sf::Sprite mSprite;
  sf::Vector2f mNormal; //This will be used to push the ball out of the wall (it'll be 'one-way', but that's fine for this type of game)
};