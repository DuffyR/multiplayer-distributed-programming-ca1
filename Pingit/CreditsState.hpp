#pragma once
//This represents the credits you'd see in a video game
//Here, it's shown what everyone did, and where did everything come from
#include "State.hpp"

#include <SFML/Graphics/Text.hpp>

#include <queue>

class CreditsState : public State {
public:
  CreditsState(StateStack& stack, Context context);

  virtual void draw();
  virtual bool update(sf::Time dt);
  virtual bool handleEvent(const sf::Event& event);

private:
  std::queue<sf::Text> mTexts;
  sf::Time mCountdown;
};