#pragma once
//A custom post effect that inverts the colors on-screen
#include "PostEffect.hpp"
#include "ResourceIdentifiers.hpp"
#include "ResourceHolder.hpp"

#include <SFML/Graphics/RenderTexture.hpp>
#include <SFML/Graphics/Shader.hpp>

class InvertEffect : public PostEffect {
public:
  InvertEffect();

  virtual void apply(const sf::RenderTexture& input, sf::RenderTarget& output);

private:
  void invert(const sf::RenderTexture& input, sf::RenderTarget& output);

private:
  ShaderHolder mShaders;
};