#pragma once
//One change from the original EmiterNode.hpp file:
//The emitter can now be given limited life

//UPDATE: Nevermind, couldn't get it to work
#include "SceneNode.hpp"
#include "Particle.hpp"

class ParticleNode;

class EmitterNode : public SceneNode {
public:
  explicit EmitterNode(Particle::Type type, sf::Time lifetime = sf::Time::Zero);

  virtual bool isMarkedForRemoval() const;

private:
  virtual void updateCurrent(sf::Time dt, CommandQueue& commands);

  void emitParticles(sf::Time dt);

private:
  sf::Time mAccumulatedTime;
  Particle::Type mType;
  ParticleNode* mParticleSystem;

  bool mHasLimitedLife;
  sf::Time mLifetime;
};