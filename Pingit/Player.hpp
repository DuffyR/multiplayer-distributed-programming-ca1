#pragma once
#include "Command.hpp"
#include "KeyBinding.hpp"

#include <SFML/System/NonCopyable.hpp>
#include <SFML/Window/Event.hpp>

#include <map>

//Forward decleration
class CommandQueue;

class Player : private sf::NonCopyable {
public:
  Player(int identifier, const KeyBinding* binding);
  void handleEvent(const sf::Event& event, CommandQueue& commands);
  void handleRealtimeInput(CommandQueue& commands);

private:
  void initializeActions();

private:
  const KeyBinding* mKeyBinding;
  std::map<PlayerAction, Command>	mActionBinding;
  int mIdentifier;
};
