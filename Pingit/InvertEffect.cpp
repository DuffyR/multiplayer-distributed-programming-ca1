#include "InvertEffect.hpp"

InvertEffect::InvertEffect()
  : mShaders()
{
  mShaders.load(ShaderIDs::InvertPass, "Media/Shaders/Fullpass.vert", "Media/Shaders/Invert.frag");
}

void InvertEffect::apply(const sf::RenderTexture & input, sf::RenderTarget & output) {
  invert(input, output);
}

void InvertEffect::invert(const sf::RenderTexture & input, sf::RenderTarget & output) {
  sf::Shader& inverter = mShaders.get(ShaderIDs::InvertPass);

  inverter.setUniform("source", input.getTexture());
  applyShader(inverter, output);
}
