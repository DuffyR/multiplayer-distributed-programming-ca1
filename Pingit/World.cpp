#include "World.hpp"
#include "TextNode.hpp"
#include "ParticleNode.hpp"
#include "SoundNode.hpp"
#include "Utility.hpp"
#include "Paddle.hpp"
#include "Ball.hpp"
#include "Wall.hpp"
#include "Powerup.hpp"
#include "Constants.hpp"
#include "DataTables.hpp"

#include <SFML/Graphics/RenderWindow.hpp>

#include <algorithm>
#include <cmath>
#include <limits>

World::World(sf::RenderTarget& outputTarget, FontHolder & fonts, SoundPlayer & sounds)
  : mTarget(outputTarget)
  , mSceneTexture()
  , mWorldView(outputTarget.getDefaultView())
  , mTextures()
  , mFonts(fonts)
  , mSounds(sounds)
  , mSceneGraph()
  , mSceneLayers()
  , mWorldBounds(0.f, 0.f, WORLD_WIDTH, WORLD_HEIGHT)
  , mSpawnPosition(mWorldBounds.width / 2.f, mWorldBounds.height / 2.f)
  , mFinishText(nullptr)
  , mBall(nullptr)
  , mFinishStateTime(sf::Time::Zero)
  , mWinningScore(3)
  , mPowerupSpawnInterval(sf::seconds(POWERUP_INTERVAL))
{
  mSceneTexture.create(mTarget.getSize().x, mTarget.getSize().y);

  loadTextures();
  buildScene();

  // Prepare the view
  mWorldView.setCenter(mSpawnPosition);
}

void World::update(sf::Time dt) {
  for (Paddle* p : mPlayerPaddles)
    p->setVelocity(0.f, 0.f);

  destroyEntitiesOutsideView();

  //Has the ball reached either sides of the screen yet
  if (hasBallLeftView()) {
    giveScore();
    advanceGame();
  }
  if (isWinningScoreReached()) {
    mFinishStateTime += dt;
  }

  // Forward commands to scene graph, adapt velocity
  while (!mCommandQueue.isEmpty())
    mSceneGraph.onCommand(mCommandQueue.pop(), dt);

  adaptEntityVelocities();

  // Collision detection and response (may destroy entities)
  handleCollisions();

  //Handle powerup spawning
  managePowerupSpawning(dt);

  mSceneGraph.removeWrecks();

  // Regular update step, adapt position (correct if outside view)
  mSceneGraph.update(dt, mCommandQueue);
  adaptEntityPositions();

  updateSounds();
}

void World::draw()
{
  if (PostEffect::isSupported()) {
    mSceneTexture.clear();
    mSceneTexture.setView(mWorldView);
    mSceneTexture.draw(mSceneGraph);
    mSceneTexture.display();

    if (isWinningScoreReached())
      mInvertEffect.apply(mSceneTexture, mTarget);
    else
      mBloomEffect.apply(mSceneTexture, mTarget);
  } else {
    mTarget.setView(mWorldView);
    mTarget.draw(mSceneGraph);
  }
}

sf::FloatRect World::getViewBounds() const
{
  return sf::FloatRect(mWorldView.getCenter() - mWorldView.getSize() / 2.f, mWorldView.getSize());
}

CommandQueue & World::getCommandQueue()
{
  return mCommandQueue;
}

Paddle* World::addPlayer(int identifier, float xOffset, sf::Vector2f normal) {
  float xPos = mSpawnPosition.x + (mWorldView.getSize().x * xOffset);

  //First the player
  std::unique_ptr<Paddle> player(new Paddle(identifier, mTextures, mFonts));
  player->setPosition(xPos, mWorldView.getCenter().y);
  player->setNormal(normal);
  mPlayerPaddles.push_back(player.get());
  
  //Then the textnode representing their score
  std::unique_ptr<TextNode> scoreDisplay(new TextNode(mFonts, std::to_string(player->getScore()), 30));
  scoreDisplay->setPosition(initializeHUDScorePositionData()[identifier]);
  mPlayerScoreDisplays.push_back(scoreDisplay.get());

  //Move the pointers into the scene graph
  mSceneLayers[UpperPlayfield]->attachChild(std::move(player));
  mSceneLayers[HUD]->attachChild(std::move(scoreDisplay));
  return mPlayerPaddles.back();
}

void World::loadTextures() {
  mTextures.load(TextureIDs::Playfield, "Media/Textures/Playfield.png");
  mTextures.load(TextureIDs::Entities, "Media/Textures/Entities_RD.png");
  mTextures.load(TextureIDs::Ball, "Media/Textures/Ball.png");
  mTextures.load(TextureIDs::Powerup, "Media/Textures/Powerup.png");
  mTextures.load(TextureIDs::Particle, "Media/Textures/Particle_RD.png");
}

void World::adaptEntityPositions() {
  for (Paddle* paddle : mPlayerPaddles) {
    sf::Vector2f position = paddle->getPosition();
    float halfHeight = paddle->getHeight() * 0.5f;
    position.y = std::max(position.y, UPPER_PADDLE_BOUNDS + halfHeight);
    position.y = std::min(position.y, LOWER_PADDLE_BOUNDS - halfHeight);
    paddle->setPosition(position);
  }
}

void World::adaptEntityVelocities()
{
  
}

void World::handleCollisions()
{
  std::set<SceneNode::Pair> collisionPairs;
  mSceneGraph.checkSceneCollision(mSceneGraph, collisionPairs);

  for (SceneNode::Pair pair : collisionPairs) {
    if (matchesCategories(pair, Category::Ball, Category::Wall)) {
      auto& ball = static_cast<Ball&>(*pair.first);
      auto& wall = static_cast<Wall&>(*pair.second);

      // Collision: Rebound the ball
      ball.reboundBallOffWall(wall);
      ball.playLocalSound(mCommandQueue, SoundEffectIDs::BallImpact);
    }
    else if (matchesCategories(pair, Category::Ball, Category::Paddle)) {
      auto& ball = static_cast<Ball&>(*pair.first);
      auto& paddle = static_cast<Paddle&>(*pair.second);

      // Collision: Rebound the ball
      ball.reboundBallOffPaddle(paddle);
      ball.playLocalSound(mCommandQueue, SoundEffectIDs::BallImpact);
    }
    else if (matchesCategories(pair, Category::Pickup, Category::Paddle)) {
      auto& powerup = static_cast<Powerup&>(*pair.first);
      auto& paddle = static_cast<Paddle&>(*pair.second);

      paddle.playLocalSound(mCommandQueue, SoundEffectIDs::Score);
      powerup.apply(paddle);
      powerup.destroy();
    }
  }
}

bool World::matchesCategories(SceneNode::Pair & colliders, Category::Type type1, Category::Type type2)
{
  unsigned int category1 = colliders.first->getCategory();
  unsigned int category2 = colliders.second->getCategory();

  // Make sure first pair entry has category type1 and second has type2
  if (type1 & category1 && type2 & category2) {
    return true;
  } else if (type1 & category2 && type2 & category1) {
    std::swap(colliders.first, colliders.second);
    return true;
  } else {
    return false;
  }
}

void World::updateSounds()
{
  sf::Vector2f listenerPosition;

  listenerPosition = mBall->getPosition();

  // Set listener's position
  mSounds.setListenerPosition(listenerPosition);

  // Remove unused sounds
  mSounds.removeStoppedSounds();
}

void World::managePowerupSpawning(sf::Time dt) {
  if (mPowerupSpawnInterval <= sf::Time::Zero) {
    mPowerupSpawnInterval = sf::seconds(POWERUP_INTERVAL);

    //Set up a new powerup
    float yPosition = WORLD_HEIGHT * 0.2f + randomInt(WORLD_HEIGHT * 0.6f);
    int chosenPlayer = 0;
    int scores[] = { mPlayerPaddles[0]->getScore(), mPlayerPaddles[1]->getScore() };
    int directions[] = {-1, 1};
    bool makeSmall = randomInt(2) == 0;

    //The scores differ; bias towards a specific player, depending on the powerup and where it'll go to
    if (scores[0] != scores[1]) {
      //The power up is 'small', it should go to the player with the higher score more often
      if (makeSmall) {
        if (scores[0] > scores[1]) {
          chosenPlayer = randomInt(3) == 0 ? 0 : 1;
        } else {
          chosenPlayer = randomInt(3) == 0 ? 1 : 0;
        }
      } else {//The power up is 'big', it should go to the player with the lower score more often
        if (scores[0] > scores[1]) {
          chosenPlayer = randomInt(3) == 0 ? 1 : 0;
        } else {
          chosenPlayer = randomInt(3) == 0 ? 0 : 1;
        }
      }
    } else { //The scores are the same; no bias
      chosenPlayer = randomInt(2);
    }

    int finalDirection = directions[chosenPlayer];

    Command powerupSpawner;
    powerupSpawner.category = Category::ScenePlayfieldLayer;
    powerupSpawner.action = [this, finalDirection, makeSmall, yPosition](SceneNode& node, sf::Time) {
      std::unique_ptr<Powerup> powerup(new Powerup(mTextures, finalDirection, makeSmall));
      powerup->setPosition(mSpawnPosition.x, yPosition);
      node.attachChild(std::move(powerup));
    };

    mCommandQueue.push(powerupSpawner);
  }

  mPowerupSpawnInterval -= dt;
}

void World::destroyEntitiesOutsideView() {
  Command command;
  command.category = Category::Pickup;
  command.action = derivedAction<Entity>([this](Entity& e, sf::Time) {
    if (!getViewBounds().intersects(e.getBoundingRect()))
      e.remove();
  });

  mCommandQueue.push(command);
}

void World::buildScene() {
  // Initialize the different layers
  for (std::size_t i = 0; i < LayerCount; ++i) {
    Category::Type category = (i == LowerPlayfield) ? Category::ScenePlayfieldLayer : Category::None;

    SceneNode::Ptr layer(new SceneNode(category));
    mSceneLayers[i] = layer.get();

    mSceneGraph.attachChild(std::move(layer));
  }

  sf::Vector2f leftcorner = getViewLeftCornerPosition(mSpawnPosition, mWorldView.getSize());

  // Prepare the tiled background
  
  sf::Texture& texture = mTextures.get(TextureIDs::Playfield);
  texture.setRepeated(true);
  sf::IntRect textureRect(0, 0, mWorldView.getSize().x, mWorldView.getSize().y);
  // Add the background sprite to the scene
  std::unique_ptr<SpriteNode> backgroundSprite(new SpriteNode(texture, textureRect));
  backgroundSprite->setPosition(leftcorner);
  mSceneLayers[Background]->attachChild(std::move(backgroundSprite));

  //Add in the two walls
  addWalls();

  //Add the ball
  std::unique_ptr<Ball> ball(new Ball(mTextures, mFonts));
  mBall = ball.get();
  mBall->setPosition(mSpawnPosition);
  mSceneLayers[UpperPlayfield]->attachChild(std::move(ball));

  //Add the 'FINISH' test
  std::unique_ptr<TextNode> finish(new TextNode(mFonts, "-FINISH-", 80));
  mFinishText = finish.get();
  mFinishText->setPosition(mSpawnPosition);
  mFinishText->setDraw(false);
  mSceneLayers[HUD]->attachChild(std::move(finish));

  // Add particle node to the scene
  std::unique_ptr<ParticleNode> smokeNode(new ParticleNode(Particle::Dust, mTextures));
  mSceneLayers[LowerPlayfield]->attachChild(std::move(smokeNode));

  // Add sound effect node
  std::unique_ptr<SoundNode> soundNode(new SoundNode(mSounds));
  mSceneGraph.attachChild(std::move(soundNode));
}

void World::addWalls() {
  const std::vector<WallPositionData> dataTable = initializeWallPositionData();

  for (auto& data : dataTable) {
    std::unique_ptr<Wall> wall(new Wall(mTextures, data.size));
    wall->setPosition(data.position);
    wall->setNormal(data.normal);
    mSceneLayers[UpperPlayfield]->attachChild(std::move(wall));
  }
}

bool World::hasBallLeftView() const {
  return !mBall->getBoundingRect().intersects(getViewBounds());
}

void World::giveScore() {
  int playerID = (mBall->getPosition().x < mSpawnPosition.x) ? 1 : 0;

  mPlayerPaddles[playerID]->increaseScore(1);
  mPlayerScoreDisplays[playerID]->setString(std::to_string(mPlayerPaddles[playerID]->getScore()));

  mBall->playLocalSound(mCommandQueue, SoundEffectIDs::Score);
}

void World::advanceGame() {
  if (isWinningScoreReached()) {
    mBall->setDraw(false);
    mBall->setIfShouldMove(false);
    mFinishText->setDraw(true);
  }

  mBall->reset();
  mBall->setPosition(mSpawnPosition);
}

bool World::isWinningScoreReached()
{
  for (Paddle* paddle : mPlayerPaddles) {
    if (paddle->getScore() >= mWinningScore) {
      return true;
    }
  }
  
  return false;
}

bool World::isGameFinished()
{
  return mFinishStateTime > sf::seconds(4.f);
}

void World::getPlayerScores(int & score1, int & score2)
{
  score1 = mPlayerPaddles[0]->getScore();
  score2 = mPlayerPaddles[1]->getScore();
}
