#include "Wall.hpp"
#include "DataTables.hpp"
#include "ResourceHolder.hpp"

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

namespace {
  TextureData Texture = initializeTextureData()[Data::Wall];
}

Wall::Wall(const TextureHolder & textures, sf::Vector2f size)
  : Entity(1)
  , mSprite(textures.get(Texture.texture), Texture.textureRect)
  , mNormal(sf::Vector2f(0.f, 1.f))
{
  mSprite.setScale(size.x / Texture.textureRect.width, size.y / Texture.textureRect.height);
}

unsigned int Wall::getCategory() const {
  return Category::Wall;
}

sf::FloatRect Wall::getBoundingRect() const {
  return getWorldTransform().transformRect(mSprite.getGlobalBounds());
}

sf::Vector2f Wall::getNormal() const {
  return mNormal;
}

void Wall::setNormal(sf::Vector2f normal) {
  mNormal = normal;
}

void Wall::drawCurrent(sf::RenderTarget & target, sf::RenderStates states) const {
  target.draw(mSprite, states);
}

void Wall::updateCurrent(sf::Time dt, CommandQueue & commands) {
  Entity::updateCurrent(dt, commands);
}
