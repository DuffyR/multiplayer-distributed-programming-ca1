#pragma once
// This is the class to represent the paddles of a 'pong' game
//Combined with Player.hpp, it is what allows the player to move the paddles to knock back the ball
#include "Entity.hpp"
#include "Command.hpp"
#include "ResourceIdentifiers.hpp"

#include <SFML/Graphics/Sprite.hpp>

#include <map>

class Paddle : public Entity {
public:
  Paddle(int identifier, const TextureHolder& textures, const FontHolder& fonts);

  virtual unsigned int	getCategory() const;
  virtual sf::FloatRect	getBoundingRect() const;
  float getMaxSpeed() const;
  float getHeight() const; //The height of the paddle. Mainly used the keep it within the game bounds, no matter its size

  void playLocalSound(CommandQueue& commands, SoundEffectIDs effect);
  int getIdentifier();
  void setIdentifier(int identifier);

  sf::Vector2f getNormal() const;
  void setNormal(sf::Vector2f normal);

  int getScore() const;
  void setScore(int score);
  void increaseScore(int score);

  void changePaddleSize(bool makeSmall); //If false, makes it big instead

private:
  enum class PaddleState { //The size of the paddle
    Regular,
    Small,
    Big
  };

private:
  virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
  virtual void updateCurrent(sf::Time dt, CommandQueue& commands);

  void setPaddleState(PaddleState state);

private:
  sf::Sprite *mActiveSprite;
  std::map<PaddleState, sf::Sprite> mSprites;
  sf::Vector2f mNormal; //This will be used to push the ball in the correct direction, so the player can't accidentally knock the ball into their own side

  int mIdentifier; //Is it player 1 or player 2?
  int mScore;
  sf::Time mPowerupTimer; //How long does its size powerup last?
};