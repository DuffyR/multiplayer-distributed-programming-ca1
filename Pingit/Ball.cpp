#include "Ball.hpp"
#include "Wall.hpp"
#include "Paddle.hpp"
#include "DataTables.hpp"
#include "Utility.hpp"
#include "CommandQueue.hpp"
#include "SoundNode.hpp"
#include "ResourceHolder.hpp"
#include "Constants.hpp"
#include "EmitterNode.hpp"
#include "Particle.hpp"

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

namespace {
  const TextureData Texture = initializeTextureData()[Data::Ball];
  const float Speed = initializeSpeedData()[Data::Ball];
  const sf::Time MoveDelay = sf::seconds(3.f);
  const float MinXSpeed = BALL_MIN_X_SPEED;
  const float StartSpeed = BALL_START_SPEED;
}

Ball::Ball(const TextureHolder & textures, const FontHolder & fonts)
  : Entity(1)
  , mStaticSprite(textures.get(Texture.texture), Texture.textureRect)
  , mRotationSprite(textures.get(TextureIDs::Ball))
  , mBallStartMoveDelay(MoveDelay)
  , mCanMove(true)
  , mCanDraw(true)
{
  mRotationSprite.setFrameSize(sf::Vector2i(32, 32));
  mRotationSprite.setNumFrames(8);
  mRotationSprite.setDuration(sf::seconds(0.5f));
  mRotationSprite.setRepeating(true);

  centerOrigin(mStaticSprite);
  centerOrigin(mRotationSprite);

  //Particle system for the trail of the ball
  std::unique_ptr<EmitterNode> smoke(new EmitterNode(Particle::Dust));
  smoke->setPosition(getPosition());
  attachChild(std::move(smoke));
}

unsigned int Ball::getCategory() const {
  return Category::Ball;
}

sf::FloatRect Ball::getBoundingRect() const {
  return getWorldTransform().transformRect(mStaticSprite.getGlobalBounds());
}

float Ball::getMaxSpeed() const {
  return Speed;
}

void Ball::setDraw(bool draw) {
  mCanDraw = draw;
}

void Ball::setIfShouldMove(bool move) {
  mCanMove = move;
}

void Ball::playLocalSound(CommandQueue & commands, SoundEffectIDs effect) {
  sf::Vector2f worldPosition = getWorldPosition();

  Command command;
  command.category = Category::SoundEffect;
  command.action = derivedAction<SoundNode>(
    [effect, worldPosition](SoundNode& node, sf::Time)
  {
    node.playSound(effect, worldPosition);
  });

  commands.push(command);
}

void Ball::reboundBallOffWall(Wall& wall) {
  sf::Vector2f velocity = getVelocity();
  velocity.y = std::abs(velocity.y) * wall.getNormal().y;
  setVelocity(velocity);
}

void Ball::reboundBallOffPaddle(Paddle& paddle) {
  sf::Vector2f velocity = getVelocity();
  velocity.x = std::abs(velocity.x) * paddle.getNormal().x;
  setVelocity(velocity.x, velocity.y);
  accelerate(0.f, (paddle.getOrigin().y - getOrigin().y) * 2.f);

  //Take in the velocity of the paddle
  accelerate(0.f, paddle.getVelocity().y);

  //Re-adjust the speed so it's not exceeding max speed
  if (length(getVelocity()) > getMaxSpeed())
    setVelocity(unitVector(getVelocity()) * getMaxSpeed());
  //But make sure it's not moving too slow horizontally
  if (std::abs(getVelocity().x) < MinXSpeed)
    setVelocity(MinXSpeed * sign(getVelocity().x), getVelocity().y);
}

void Ball::drawCurrent(sf::RenderTarget & target, sf::RenderStates states) const {
  if (mCanDraw) {
    if (length(getVelocity()) != 0.f)
      target.draw(mRotationSprite, states);
    else
      target.draw(mStaticSprite, states);
  }
}

void Ball::updateCurrent(sf::Time dt, CommandQueue & commands) {
  //Delay the ball from moving (if it even should move in the first place)
  if (mBallStartMoveDelay > sf::seconds(0.f) && mCanMove) {
    mBallStartMoveDelay -= dt;

    if (mBallStartMoveDelay <= sf::seconds(0.f)) {
      startBallMove();
    }
  }
  mRotationSprite.update(dt);

  Entity::updateCurrent(dt, commands);
}

void Ball::startBallMove() {
  float radians = toRadians(randomInt(45) - 22.5f);
  if (randomInt(2) == 1) { radians += toRadians(180.f); }
  float vx = StartSpeed * std::cos(radians);
  float vy = StartSpeed * -std::sin(radians);

  mRotationSprite.restart();
  setVelocity(vx, vy);
}

void Ball::reset() {
  setVelocity(sf::Vector2f());

  mBallStartMoveDelay = MoveDelay;
}
