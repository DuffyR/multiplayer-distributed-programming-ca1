#pragma once
#include "State.hpp"
#include "World.hpp"
#include "Player.hpp"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>

class GameState : public State
{
public:
  GameState(StateStack& stack, Context context);

  virtual void draw();
  virtual bool update(sf::Time dt);
  virtual bool handleEvent(const sf::Event& event);

private:
  void setupPlayers();

  void drawWaitingText();

private:
  typedef std::unique_ptr<Player> PlayerPtr;

private:
  World mWorld;
  std::map<int, PlayerPtr> mPlayers;

  bool mArePlayersReady;
  sf::Text mWaitForPlayerText;
};
