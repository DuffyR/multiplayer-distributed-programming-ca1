#include "GameState.hpp"
#include "MusicPlayer.hpp"
#include "Utility.hpp"

#include <SFML/Graphics/RenderWindow.hpp>

GameState::GameState(StateStack & stack, Context context)
  : State(stack, context)
  , mWorld(*context.window, *context.fonts, *context.sounds)
  , mArePlayersReady(false)
  , mWaitForPlayerText()
{
  setupPlayers();

  *context.scoreRef1 = 0;
  *context.scoreRef2 = 0;
  context.music->play(MusicIDs::GameTheme);

  mWaitForPlayerText.setFont(context.fonts->get(FontIDs::Main));
  mWaitForPlayerText.setString("PRESS -SPACE-\nWHEN BOTH PLAYERS ARE READY\nFIRST TO THREE WIN");
  centerOrigin(mWaitForPlayerText);
  mWaitForPlayerText.setPosition(sf::Vector2f(context.window->getSize() / 2u));
}

void GameState::draw() {
  mWorld.draw();
  
  drawWaitingText();
}

bool GameState::update(sf::Time dt) {
  if (!mArePlayersReady) { return true; }

  mWorld.update(dt);

  if (mWorld.isGameFinished()) {
    mWorld.getPlayerScores(*getContext().scoreRef1, *getContext().scoreRef2);
    requestStateClear();
    requestStackPush(StateIDs::Results);
  }

  CommandQueue& commands = mWorld.getCommandQueue();
  for (auto& pair : mPlayers)
    pair.second->handleRealtimeInput(commands);

  return true;
}

bool GameState::handleEvent(const sf::Event & event) {
  //A key was pressed, but which one?
  if (event.type == sf::Event::KeyPressed) {
    //Space pressed, start the game since the players are ready
    if (event.key.code == sf::Keyboard::Space && !mArePlayersReady) {
      mArePlayersReady = true;
    } else if (event.key.code == sf::Keyboard::Escape && mArePlayersReady && !mWorld.isWinningScoreReached()) {
      requestStackPush(StateIDs::Pause);
    }
  }

  return true;
}

void GameState::setupPlayers() {
  //Ignore the first elements in each array
  float xOffsets[2] = {-0.4f, 0.4f};
  KeyBinding* bindings[2] = {getContext().keys1, getContext().keys2};
  sf::Vector2f normals[2] = {sf::Vector2f(1.f, 0.f), sf::Vector2f(-1.f, 0.f)};

  for (size_t i = 0; i < 2; i++) {
    Paddle* paddle = mWorld.addPlayer(i, xOffsets[i], normals[i]);
    mPlayers[i].reset(new Player(i, bindings[i]));
  }
}

void GameState::drawWaitingText() {
  if (mArePlayersReady) { return; }

  sf::RenderWindow& window = *getContext().window;
  window.draw(mWaitForPlayerText);
}
