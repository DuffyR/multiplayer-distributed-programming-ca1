#pragma once
#include "ResourceHolder.hpp"
#include "ResourceIdentifiers.hpp"
#include "SceneNode.hpp"
#include "SpriteNode.hpp"
#include "CommandQueue.hpp"
#include "Command.hpp"
#include "BloomEffect.hpp"
#include "InvertEffect.hpp"
#include "SoundPlayer.hpp"

#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics/View.hpp>
#include <SFML/Graphics/Texture.hpp>

#include <array>
#include <queue>

// Forward declaration
namespace sf {
  class RenderTarget;
}
class Paddle;
class Ball;
class TextNode;

class World : private sf::NonCopyable {
public:
  World(sf::RenderTarget& outputTarget, FontHolder& fonts, SoundPlayer& sounds);
  void update(sf::Time dt);
  void draw();

  sf::FloatRect getViewBounds() const;
  CommandQueue& getCommandQueue();

  Paddle* addPlayer(int identifier, float xOffset, sf::Vector2f normal);
  bool isWinningScoreReached();
  bool isGameFinished();
  void getPlayerScores(int& score1, int& score2);

private:
  void loadTextures();
  void adaptEntityPositions();
  void adaptEntityVelocities();
  void handleCollisions();
  bool matchesCategories(SceneNode::Pair& colliders, Category::Type type1, Category::Type type2);
  void updateSounds();

  void managePowerupSpawning(sf::Time dt);
  void destroyEntitiesOutsideView();

  void buildScene();
  void addWalls();

  bool hasBallLeftView() const;
  void giveScore();
  void advanceGame();

private:
  enum Layer {
    Background,
    UpperPlayfield,
    LowerPlayfield,
    HUD,
    LayerCount
  };

private:
  sf::RenderTarget& mTarget;
  sf::RenderTexture mSceneTexture;
  BloomEffect mBloomEffect;
  InvertEffect mInvertEffect;
  sf::View mWorldView;
  TextureHolder mTextures;
  FontHolder& mFonts;
  SoundPlayer& mSounds;

  SceneNode mSceneGraph;
  std::array<SceneNode*, LayerCount> mSceneLayers;
  CommandQueue mCommandQueue;

  sf::FloatRect mWorldBounds;
  sf::Vector2f mSpawnPosition;
  std::vector<Paddle*> mPlayerPaddles;
  std::vector<TextNode*> mPlayerScoreDisplays;
  TextNode* mFinishText;
  sf::Time mFinishStateTime;
  Ball* mBall;
  int mWinningScore;

  sf::Time mPowerupSpawnInterval; //The rate at which powerups spawn
};
