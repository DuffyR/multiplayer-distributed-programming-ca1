#pragma once
#include <SFML/Window/Keyboard.hpp>
#include <SFML/System/Vector2.hpp>

#include <sstream>

//Forward declerations
namespace sf {
  class Sprite;
  class Text;
  class View;
}

class Animation;

// Convert enumerators to strings
std::string toString(sf::Keyboard::Key key);

// Call setOrigin() with the center of the object
void centerOrigin(sf::Sprite& sprite);
void centerOrigin(sf::Text& text);
void centerOrigin(Animation& animation);

// Degree/radian conversion
float toDegrees(float radians);
float toRadians(float degrees);

// Random number generation
int randomInt(int exclusiveMax);

// Vector operations
float length(sf::Vector2f vector);
sf::Vector2f unitVector(sf::Vector2f vector);

sf::Vector2f getViewLeftCornerPosition(sf::Vector2f& viewCentre, const sf::Vector2f& viewSize);

int sign(int num);
int sign(float num);