#pragma once
//Very simple state; just shows instructions on a static image
//Probably shouldn't be its own state, but ironically, I don't have enough time to make THAT work
#include "State.hpp"

#include <SFML/Graphics/Sprite.hpp>

class TutorialState : public State {
public:
  TutorialState(StateStack& stack, Context context);

  virtual void draw();
  virtual bool update(sf::Time dt);
  virtual bool handleEvent(const sf::Event& event);

private:
  sf::Sprite mBackgroundSprite;
};