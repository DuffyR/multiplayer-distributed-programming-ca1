#include "EmitterNode.hpp"
#include "ParticleNode.hpp"
#include "CommandQueue.hpp"
#include "Command.hpp"

EmitterNode::EmitterNode(Particle::Type type, sf::Time lifetime)
  : SceneNode()
  , mAccumulatedTime(sf::Time::Zero)
  , mType(type)
  , mParticleSystem(nullptr)
  , mHasLimitedLife(false)
  , mLifetime(sf::Time::Zero)
{
  if (lifetime > sf::Time::Zero) {
    mHasLimitedLife = true;
    mLifetime = lifetime;
  }
}

bool EmitterNode::isMarkedForRemoval() const {
  if (!mHasLimitedLife)
    return false;
  else {
    return mLifetime <= sf::Time::Zero;
  }
}

void EmitterNode::updateCurrent(sf::Time dt, CommandQueue & commands) {
  if (mParticleSystem) {
    emitParticles(dt);

    if (mHasLimitedLife)
      mLifetime -= dt;
  } else {
    // Find particle node with the same type as emitter node
    auto finder = [this](ParticleNode& container, sf::Time) {
      if (container.getParticleType() == mType)
        mParticleSystem = &container;
    };

    Command command;
    command.category = Category::ParticleSystem;
    command.action = derivedAction<ParticleNode>(finder);

    commands.push(command);
  }
}

void EmitterNode::emitParticles(sf::Time dt) {
  const float emissionRate = 30.f;
  const sf::Time interval = sf::seconds(1.f) / emissionRate;

  mAccumulatedTime += dt;

  while (mAccumulatedTime > interval) {
    mAccumulatedTime -= interval;
    mParticleSystem->addParticle(getWorldPosition());
  }
}
