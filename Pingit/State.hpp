#pragma once
#include "StateIdentifiers.hpp"
#include "ResourceIdentifiers.hpp"

#include <SFML/System/Time.hpp>
#include <SFML/Window/Event.hpp>

#include <memory>

//Forward declerations
namespace sf {
  class RenderWindow;
}

class StateStack;
class MusicPlayer;
class SoundPlayer;
class KeyBinding;

class State {
public: // Typedef & Struct
  typedef std::unique_ptr<State> Ptr;

  struct Context {
	Context(sf::RenderWindow& window, TextureHolder& textures, FontHolder& fonts,
		MusicPlayer& music, SoundPlayer& sounds, KeyBinding& keys1, KeyBinding& keys2,
      int& scoreRef1, int& scoreRef2);

	sf::RenderWindow* window;
	TextureHolder* textures;
	FontHolder* fonts;
	MusicPlayer* music;
	SoundPlayer* sounds;
	KeyBinding* keys1;
	KeyBinding* keys2;
    int* scoreRef1;
    int* scoreRef2;
  };

public: //Methods
  State(StateStack& stack, Context context);
  virtual ~State();

  virtual void draw() = 0;
  virtual bool update(sf::Time dt) = 0;
  virtual bool handleEvent(const sf::Event& event) = 0;

protected: //Methods
  void requestStackPush(StateIDs stateID);
  void requestStackPop();
  void requestStateClear();

  Context getContext() const;

private: //Variables
  StateStack* mStack;
  Context mContext;
};