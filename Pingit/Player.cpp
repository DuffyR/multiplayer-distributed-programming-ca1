#include "Player.hpp"
#include "CommandQueue.hpp"
#include "Paddle.hpp"

#include <map>
#include <string>
#include <algorithm>

struct PaddleMover {
  PaddleMover(float vy, int identifier) 
  : yVelocity(vy), paddleID(identifier)
  {}

  void operator() (Paddle& paddle, sf::Time) const {
    if (paddle.getIdentifier() == paddleID)
      paddle.accelerate(0.f, yVelocity);
  }

  float yVelocity;
  int paddleID;
};

Player::Player(int identifier, const KeyBinding * binding)
  : mKeyBinding(binding), mIdentifier(identifier)
{
  // Set initial action bindings
  initializeActions();

  // Assign all categories to player's aircraft
  for (auto& pair : mActionBinding)
    pair.second.category = Category::Paddle;
}

void Player::handleEvent(const sf::Event & event, CommandQueue & commands)
{
  if (event.type == sf::Event::KeyPressed) {
    PlayerAction action;
    if (mKeyBinding && mKeyBinding->checkAction(event.key.code, action) && !isRealtimeAction(action))
      commands.push(mActionBinding[action]);
  }
}

void Player::handleRealtimeInput(CommandQueue & commands)
{
  std::vector<PlayerAction> activeActions = mKeyBinding->getRealtimeActions();
  for (PlayerAction action : activeActions)
    commands.push(mActionBinding[action]);
}

void Player::initializeActions()
{
  mActionBinding[PlayerAction::MoveUp].action = derivedAction<Paddle>(PaddleMover(-200, mIdentifier));
  mActionBinding[PlayerAction::MoveDown].action = derivedAction<Paddle>(PaddleMover(200, mIdentifier));
}
