#include "TutorialState.hpp"
#include "Utility.hpp"
#include "ResourceHolder.hpp"

#include <SFML/Graphics/RenderWindow.hpp>

TutorialState::TutorialState(StateStack & stack, Context context)
  : State(stack, context)
{
  mBackgroundSprite.setTexture(context.textures->get(TextureIDs::Tutorial));
}

void TutorialState::draw() {
  sf::RenderWindow& window = *getContext().window;
  window.draw(mBackgroundSprite);
}

bool TutorialState::update(sf::Time dt) {
  return false; //Update what?
}

bool TutorialState::handleEvent(const sf::Event & event) {
  sf::Keyboard::Key permittedKeys[3] = { sf::Keyboard::Space, sf::Keyboard::Enter, sf::Keyboard::Escape };

  if (event.type == sf::Event::KeyPressed) {
    sf::Keyboard::Key* found = std::find(std::begin(permittedKeys), std::end(permittedKeys), event.key.code);

    if (found != std::end(permittedKeys))
      requestStackPop();
  }

  return false;
}
