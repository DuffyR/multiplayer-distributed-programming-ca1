#include "SplashState.hpp"
#include "Utility.hpp"
#include "SoundPlayer.hpp"
#include "ResourceHolder.hpp"

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

SplashState::SplashState(StateStack & stack, Context context)
  : State(stack, context)
  , mTexts()
  , mCountdown(sf::seconds(0.5f))
  , mPhase(0)
  , mMyNameStorage("RONAN DUFFY")
{
  //Assign common data across the elements in the text array
  for (auto& text : mTexts) {
    text.setFont(context.fonts->get(FontIDs::Main));
    text.setFillColor(sf::Color(15, 56, 15));
  }

  //Apply more element-specific data in the text array
  mTexts[GameBy].setCharacterSize(25u);
  mTexts[GameBy].setPosition(sf::Vector2f(context.window->getSize() / 2u));
  mTexts[GameBy].move(0.f, context.window->getSize().y * -0.2f);

  mTexts[MyName].setCharacterSize(50u);
  mTexts[MyName].setPosition(sf::Vector2f(context.window->getSize() / 2u));
}

void SplashState::draw() {
  sf::RenderWindow& window = *getContext().window;
  window.setView(window.getDefaultView());

  // Create green-ish background
  sf::RectangleShape backgroundShape;
  backgroundShape.setFillColor(sf::Color(155, 188, 15));
  backgroundShape.setSize(window.getView().getSize());
  window.draw(backgroundShape);

  //Draw the texts
  for (auto& text : mTexts)
    window.draw(text);
}

bool SplashState::update(sf::Time dt) {
  mCountdown -= dt;

  if (mCountdown <= sf::Time::Zero)
    advance();

  return false;
}

bool SplashState::handleEvent(const sf::Event & event) {
  //The player can go to the next screen if they press particular buttons
  //This will skip the SplashState sequence
  sf::Keyboard::Key permittedKeys[3] = {sf::Keyboard::Space, sf::Keyboard::Enter, sf::Keyboard::Escape};

  if (event.type == sf::Event::KeyPressed) {
    sf::Keyboard::Key* found = std::find(std::begin(permittedKeys), std::end(permittedKeys), event.key.code);

    if (found != std::end(permittedKeys)) {
      mPhase = 2;
      mCountdown = sf::Time::Zero;
    }
  }

  return false;
}

void SplashState::advance() {
  sf::Time newInterval;

  switch (mPhase) {
  case 0:
    applyText("A GAME BY", GameBy);
    newInterval = sf::seconds(1.f);
    playSound(SoundEffectIDs::MenuSelection);
    break;
  case 1:
    showNextLetter();
    newInterval = (mTexts[MyName].getString() == mMyNameStorage) ? sf::seconds(3.f) : sf::seconds(0.3f);
    playSound(SoundEffectIDs::Bip);
    break;
  case 2:
    requestStateClear();
    requestStackPush(StateIDs::Title);
    break;
  }

  mCountdown = newInterval;

  //If mPhase is at '1', we only advance it if we have the full string in the array index
  //For every other phase, we just advance
  if (mPhase == 1) {
    mPhase = (mTexts[MyName].getString() == mMyNameStorage) ? ++mPhase : mPhase;
  } else {
    mPhase++;
  }
}

void SplashState::applyText(const std::string & text, TextIDs index) {
  mTexts[index].setString(text);
  centerOrigin(mTexts[index]);
}

void SplashState::showNextLetter() {
  size_t stringSize = mTexts[MyName].getString().getSize();
  std::string newText = mMyNameStorage.substr(0, stringSize + 1);
  applyText(newText, MyName);
}

void SplashState::playSound(SoundEffectIDs sound) {
  getContext().sounds->play(sound);
}

