#pragma once
#include "State.hpp"
#include "Animation.hpp"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>

#include <array>

class TitleState : public State {
public:
  TitleState(StateStack& stack, Context context);

  virtual void draw();
  virtual bool update(sf::Time dt);
  virtual bool handleEvent(const sf::Event& event);

private:
  void addDecorativeBall(sf::Vector2f position);

private:
  sf::Sprite mBackgroundSprite;
  sf::Text mText;
  std::vector<Animation> mDecorativeBalls;

  bool mShowText;
  sf::Time mTextEffectTime; 
};
