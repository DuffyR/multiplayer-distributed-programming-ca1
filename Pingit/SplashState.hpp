#pragma once
//This represents those screens you see before you even get to the title screen of a game,
//displaying those who were involved in the game
#include "State.hpp"

#include <SFML/Graphics/Text.hpp>

#include <array>

class SplashState : public State {
public:
  SplashState(StateStack& stack, Context context);

  virtual void draw();
  virtual bool update(sf::Time dt);
  virtual bool handleEvent(const sf::Event& event);

private:
  enum TextIDs {
    GameBy,
    MyName,
    TextCount
  };

private:
  void advance();
  void applyText(const std::string& text, TextIDs index);

  void showNextLetter();

  void playSound(SoundEffectIDs sound);

private:
  std::array<sf::Text, TextCount> mTexts;

  //These two variables handle the progression of this state
  sf::Time mCountdown;
  int mPhase;

  //This variable holds my name normally. When its brought over to mTexts, it'll be shown one letter at a time
  std::string mMyNameStorage;
};