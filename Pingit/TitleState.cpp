#include "TitleState.hpp"
#include "Utility.hpp"
#include "ResourceHolder.hpp"

#include <SFML/Graphics/RenderWindow.hpp>

TitleState::TitleState(StateStack & stack, Context context)
  : State(stack, context)
  , mText()
  , mShowText(true)
  , mTextEffectTime(sf::Time::Zero)
{
  mBackgroundSprite.setTexture(context.textures->get(TextureIDs::TitleScreen));

  mText.setFont(context.fonts->get(FontIDs::Main));
  mText.setString("Press any key to start");
  centerOrigin(mText);
  mText.setFillColor(sf::Color(15, 56, 15));
  mText.setPosition(sf::Vector2f(context.window->getSize() / 2u));

  addDecorativeBall(sf::Vector2f(263.f, 55.f));
  addDecorativeBall(sf::Vector2f(423.f, 55.f));
}

void TitleState::draw()
{
  sf::RenderWindow& window = *getContext().window;
  window.draw(mBackgroundSprite);

  if (mShowText)
    window.draw(mText);

  for (auto& ball : mDecorativeBalls)
    window.draw(ball);
}

bool TitleState::update(sf::Time dt)
{
  mTextEffectTime += dt;

  if (mTextEffectTime >= sf::seconds(0.5f))
  {
    mShowText = !mShowText;
    mTextEffectTime = sf::Time::Zero;
  }

  for (auto& ball : mDecorativeBalls)
    ball.update(dt);

  return true;
}

bool TitleState::handleEvent(const sf::Event & event)
{
  // If any key is pressed, trigger the next screen
  if (event.type == sf::Event::KeyPressed)
  {
    requestStackPop();
    requestStackPush(StateIDs::Menu);
  }

  return true;
}

void TitleState::addDecorativeBall(sf::Vector2f position)
{
  Animation ball(getContext().textures->get(TextureIDs::Ball));
  ball.setFrameSize(sf::Vector2i(32, 32));
  ball.setNumFrames(8);
  ball.setDuration(sf::seconds(2));
  ball.setRepeating(true);
  centerOrigin(ball);
  ball.setPosition(position);
  mDecorativeBalls.push_back(ball);
}
