#include "TextNode.hpp"
#include "Utility.hpp"

#include <SFML/Graphics/RenderTarget.hpp>

TextNode::TextNode(const FontHolder & fonts, const std::string & text, int textSize)
  : mCanDraw(true)
{
  mText.setFont(fonts.get(FontIDs::Main));
  mText.setCharacterSize(textSize);
  setString(text);
}

void TextNode::setString(const std::string & text)
{
  mText.setString(text);
  centerOrigin(mText);
}

void TextNode::setDraw(bool flag)
{
  mCanDraw = flag;
}

void TextNode::drawCurrent(sf::RenderTarget & target, sf::RenderStates states) const
{
  if (mCanDraw)
    target.draw(mText, states);
}
