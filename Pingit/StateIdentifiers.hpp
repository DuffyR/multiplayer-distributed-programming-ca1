#pragma once
enum class StateIDs {
	None,
	Splash,
	Title,
	Menu,
	Game,
    Results,
	Settings,
	Pause,
    Credits,
    Tutorial
};