#pragma once
//Similar to GameOverState form the example game
//The differecnes are as a result of this game being different from the example game
//It displays the two players' scores
#include "State.hpp"
#include "Container.hpp"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>

class ResultsState : public State {
public:
  ResultsState(StateStack& stack, Context context);

  virtual void draw();
  virtual bool update(sf::Time dt);
  virtual bool handleEvent(const sf::Event& event);

private:
  void advance();
  void addText(const std::string& text, float xPos, float yPos, unsigned int charSize);
  const std::string determineWinner();

  void playSound(SoundEffectIDs sound);

private:
  std::vector<sf::Text> mTexts;
  sf::Time mCountdownTime;
  int mPhase;
};