#pragma once

// Entity/scene node category, used to dispatch commands
// Not using 'enum class' here; don't want to use 'static_cast' all the time
namespace Category {
  enum Type {
	None = 0,
	ScenePlayfieldLayer = 1 << 0,
	Paddle = 1 << 1,
    Wall = 1 << 2,
	Pickup = 1 << 3,
	Ball = 1 << 4,
	ParticleSystem = 1 << 5,
	SoundEffect = 1 << 6
  };
}