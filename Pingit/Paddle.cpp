#include "Paddle.hpp"
#include "DataTables.hpp"
#include "Utility.hpp"
#include "CommandQueue.hpp"
#include "SoundNode.hpp"
#include "ResourceHolder.hpp"

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

#include <cmath>

namespace {
  std::vector<TextureData> extactPaddleTextureData() {
    std::vector<TextureData> textureTable = initializeTextureData();

    std::vector<TextureData> extractedTable;
    extractedTable.push_back(textureTable[Data::PaddleP1]);
    extractedTable.push_back(textureTable[Data::PaddleP2]);
    extractedTable.push_back(textureTable[Data::PaddleP1Small]);
    extractedTable.push_back(textureTable[Data::PaddleP2Small]);
    extractedTable.push_back(textureTable[Data::PaddleP1Big]);
    extractedTable.push_back(textureTable[Data::PaddleP2Big]);

    return extractedTable;
  }

  const std::vector<TextureData> TextureTable = extactPaddleTextureData();
  const float Speed = initializeSpeedData()[Data::PaddleP1];
  const float PowerupDuration = 8.f; //In seconds
}

Paddle::Paddle(int identifier, const TextureHolder & textures, const FontHolder & fonts)
  : Entity(1)
  , mActiveSprite(nullptr)
  , mIdentifier(identifier)
  , mScore(0)
  , mPowerupTimer(sf::Time::Zero)
{
  //centerOrigin(mSprite);
  mSprites[PaddleState::Regular] = sf::Sprite(textures.get(TextureTable[identifier].texture), TextureTable[identifier].textureRect);
  mSprites[PaddleState::Small] = sf::Sprite(textures.get(TextureTable[identifier + 2].texture), TextureTable[identifier + 2].textureRect);
  mSprites[PaddleState::Big] = sf::Sprite(textures.get(TextureTable[identifier + 4].texture), TextureTable[identifier + 4].textureRect);
  for (auto& sprite : mSprites)
    centerOrigin(sprite.second);

  setPaddleState(PaddleState::Regular);
}

unsigned int Paddle::getCategory() const {
  return Category::Paddle;
}

sf::FloatRect Paddle::getBoundingRect() const {
  return getWorldTransform().transformRect(mActiveSprite->getGlobalBounds());
}

float Paddle::getMaxSpeed() const {
  return Speed;
}

float Paddle::getHeight() const
{
  return getBoundingRect().height;
}

void Paddle::playLocalSound(CommandQueue & commands, SoundEffectIDs effect) {
  sf::Vector2f worldPosition = getWorldPosition();

  Command command;
  command.category = Category::SoundEffect;
  command.action = derivedAction<SoundNode>(
    [effect, worldPosition](SoundNode& node, sf::Time)
  {
    node.playSound(effect, worldPosition);
  });

  commands.push(command);
}

int Paddle::getIdentifier() {
  return mIdentifier;
}

void Paddle::setIdentifier(int identifier) {
  mIdentifier = identifier;
}

sf::Vector2f Paddle::getNormal() const {
  return mNormal;
}

void Paddle::setNormal(sf::Vector2f normal) {
  mNormal = normal;
}

int Paddle::getScore() const {
  return mScore;
}

void Paddle::setScore(int score) {
  mScore = score;
}

void Paddle::increaseScore(int score) {
  mScore += score;
}

void Paddle::changePaddleSize(bool makeSmall) {
  PaddleState newState = makeSmall ? PaddleState::Small : PaddleState::Big;
  setPaddleState(newState);
  mPowerupTimer = sf::seconds(PowerupDuration);
}

void Paddle::drawCurrent(sf::RenderTarget & target, sf::RenderStates states) const {
  target.draw(*mActiveSprite, states);
}

void Paddle::updateCurrent(sf::Time dt, CommandQueue & commands) {
  if (mPowerupTimer > sf::Time::Zero) {
    mPowerupTimer -= dt;
    if (mPowerupTimer <= sf::Time::Zero) {
      setPaddleState(PaddleState::Regular);
    }
  }

  Entity::updateCurrent(dt, commands);
}

void Paddle::setPaddleState(PaddleState state) {
  mActiveSprite = &mSprites[state];
}
