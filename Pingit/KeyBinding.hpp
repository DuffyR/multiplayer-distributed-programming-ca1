#pragma once
#include <SFML/Window/Keyboard.hpp>

#include <map>
#include <vector>

enum class PlayerAction { MoveUp, MoveDown, Count };

class KeyBinding {
public:
  explicit KeyBinding(int controlPreconfiguration);

  void assignKey(PlayerAction action, sf::Keyboard::Key key);
  sf::Keyboard::Key getAssignedKey(PlayerAction action) const;

  bool checkAction(sf::Keyboard::Key key, PlayerAction& out) const;
  std::vector<PlayerAction> getRealtimeActions() const;

private:
  std::map<sf::Keyboard::Key, PlayerAction> mKeyMap;
};

bool isRealtimeAction(PlayerAction action);
