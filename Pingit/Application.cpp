#include "Application.hpp"
#include "Utility.hpp"
#include "State.hpp"
#include "StateIdentifiers.hpp"
#include "TitleState.hpp"
#include "MenuState.hpp"
#include "GameState.hpp"
#include "PauseState.hpp"
#include "SettingsState.hpp"
#include "ResultsState.hpp"
#include "SplashState.hpp"
#include "CreditsState.hpp"
#include "TutorialState.hpp"

const sf::Time Application::TimePerFrame = sf::seconds(1.f / 60.f);

Application::Application()
  : mWindow(sf::VideoMode(640, 480), "Pingit", sf::Style::Close)
  , mTextures()
  , mFonts()
  , mMusic()
  , mSounds()
  , mKeyBinding1(1)
  , mKeyBinding2(2)
  , mScoreRef1(0)
  , mScoreRef2(0)
  , mStateStack(State::Context(mWindow, mTextures, mFonts, mMusic, mSounds, mKeyBinding1, mKeyBinding2, mScoreRef1, mScoreRef2))
  , mStatisticsText()
  , mStatisticsUpdateTime()
  , mStatisticsNumFrames(0)
{
  mWindow.setKeyRepeatEnabled(false);
  mWindow.setVerticalSyncEnabled(true);

  mFonts.load(FontIDs::Main, "Media/8bitOperatorPlusSC-Bold.ttf");

  mTextures.load(TextureIDs::TitleScreen, "Media/Textures/TitleScreen_RD.png");
  mTextures.load(TextureIDs::Tutorial, "Media/Textures/HowToPlay.png");
  mTextures.load(TextureIDs::Buttons, "Media/Textures/Buttons_RD.png");
  mTextures.load(TextureIDs::Ball, "Media/Textures/Ball.png");

  mStatisticsText.setFont(mFonts.get(FontIDs::Main));
  mStatisticsText.setPosition(5.f, 5.f);
  mStatisticsText.setCharacterSize(10u);

  registerStates();
  mStateStack.pushState(StateIDs::Splash);

  mMusic.setVolume(25.f);
}

void Application::run()
{
  sf::Clock clock;
  sf::Time timeSinceLastUpdate = sf::Time::Zero;

  while (mWindow.isOpen())
  {
    sf::Time dt = clock.restart();
    timeSinceLastUpdate += dt;
    while (timeSinceLastUpdate > TimePerFrame)
    {
      timeSinceLastUpdate -= TimePerFrame;

      processInput();
      update(TimePerFrame);

      // Check inside this loop, because stack might be empty before update() call
      if (mStateStack.isEmpty())
        mWindow.close();
    }

    updateStatistics(dt);
    render();
  }
}

void Application::processInput()
{
  sf::Event event;
  while (mWindow.pollEvent(event))
  {
    mStateStack.handleEvent(event);

    if (event.type == sf::Event::Closed)
      mWindow.close();
  }
}

void Application::update(sf::Time dt)
{
  mStateStack.update(dt);
}

void Application::render()
{
  mWindow.clear();

  mStateStack.draw();

  mWindow.setView(mWindow.getDefaultView());
  mWindow.draw(mStatisticsText);

  mWindow.display();
}

void Application::updateStatistics(sf::Time dt)
{
  mStatisticsUpdateTime += dt;
  mStatisticsNumFrames += 1;
  if (mStatisticsUpdateTime >= sf::seconds(1.0f))
  {
    mStatisticsText.setString("FPS: " + std::to_string(mStatisticsNumFrames));

    mStatisticsUpdateTime -= sf::seconds(1.0f);
    mStatisticsNumFrames = 0;
  }
}

void Application::registerStates()
{
  mStateStack.registerState<TitleState>(StateIDs::Title);
  mStateStack.registerState<MenuState>(StateIDs::Menu);
  mStateStack.registerState<GameState>(StateIDs::Game);
  mStateStack.registerState<PauseState>(StateIDs::Pause);
  mStateStack.registerState<SettingsState>(StateIDs::Settings);
  mStateStack.registerState<ResultsState>(StateIDs::Results);
  mStateStack.registerState<SplashState>(StateIDs::Splash);
  mStateStack.registerState<CreditsState>(StateIDs::Credits);
  mStateStack.registerState<TutorialState>(StateIDs::Tutorial);
}
