#include "State.hpp"
#include "StateStack.hpp"

State::State(StateStack & stack, Context context)
  : mStack(&stack) , mContext(context)
{
}

State::~State()
{
}

void State::requestStackPush(StateIDs stateID) {
  mStack->pushState(stateID);
}

void State::requestStackPop()
{
  mStack->popState();
}

void State::requestStateClear() {
  mStack->clearStates();
}

State::Context State::getContext() const
{
	return mContext;
}

State::Context::Context(sf::RenderWindow & window, TextureHolder & textures, FontHolder & fonts, MusicPlayer & music, SoundPlayer & sounds, KeyBinding & keys1, KeyBinding & keys2, int& scoreRef1, int& scoreRef2)
  : window(&window)
  , textures(&textures)
  , fonts(&fonts)
  , music(&music)
  , sounds(&sounds)
  , keys1(&keys1)
  , keys2(&keys2)
  , scoreRef1(&scoreRef1)
  , scoreRef2(&scoreRef2)
{
}
