#pragma once
#include "ResourceHolder.hpp"
#include "ResourceIdentifiers.hpp"
#include "SceneNode.hpp"

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Font.hpp>

class TextNode : public SceneNode {
public:
  TextNode(const FontHolder& fonts, const std::string& text, int textSize = 20);

  void setString(const std::string& text);
  void setDraw(bool flag);

private:
  virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;

private:
  sf::Text mText;
  bool mCanDraw;
};