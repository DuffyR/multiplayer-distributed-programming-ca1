#pragma once
#include "ResourceIdentifiers.hpp"

#include <SFML/System/Time.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/Rect.hpp>

#include <vector>
#include <functional>

//Forward declaration
class Paddle;

//Identifiers for each type of entity in game
//Only used for data stuff, so seperate from Category.hpp
namespace Data {
  enum ID {
    PaddleP1,
    PaddleP1Small,
    PaddleP1Big,
    PaddleP2,
    PaddleP2Small,
    PaddleP2Big,
    Ball,
    Wall,
    Pickup,
    EntityTypeCount
  };
}

struct TextureData {
  TextureIDs texture;
  sf::IntRect textureRect;
};

struct WallPositionData {
  sf::Vector2f position;
  sf::Vector2f size;
  sf::Vector2f normal; //This will be used to push the ball out of the wall (it'll be 'one-way', but that's fine for this type of game)
};

struct ParticleData {
  sf::Color color;
  sf::Time lifetime;
};

std::vector<TextureData> initializeTextureData();
std::vector<WallPositionData> initializeWallPositionData();
std::vector<float> initializeSpeedData();
std::vector<ParticleData>	initializeParticleData();
std::vector<sf::Vector2f> initializeHUDScorePositionData();