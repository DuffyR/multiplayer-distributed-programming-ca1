#pragma once
#include "State.hpp"
#include "Container.hpp"
#include "Animation.hpp"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>

class MenuState : public State{
public:
  MenuState(StateStack& stack, Context context);

  virtual void draw();
  virtual bool update(sf::Time dt);
  virtual bool handleEvent(const sf::Event& event);

private:
  void addDecorativeBall(sf::Vector2f position);

private:
  sf::Sprite mBackgroundSprite;
  std::vector<Animation> mDecorativeBalls;
  GUI::Container mGUIContainer;
};
