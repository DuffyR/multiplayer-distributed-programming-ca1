#pragma once

// Forward declaration of SFML classes
namespace sf {
  class Texture;
  class Font;
  class Shader;
  class SoundBuffer;
}

enum class TextureIDs {
  Entities,
  Ball,
  Powerup,
  Playfield,
  TitleScreen,
  Tutorial,
  Particle,
  Buttons
};

enum class ShaderIDs {
  BrightnessPass,
  DownSamplePass,
  GaussianBlurPass,
  AddPass,
  InvertPass
};

enum class FontIDs {
  Main
};

enum class SoundEffectIDs {
  MenuSelection,
  Bip,
  BallImpact,
  Score,
  GameWon
};

enum class MusicIDs {
  MenuTheme,
  GameTheme
};

// Forward declaration and a few type definitions
template <typename Resource, typename Identifier>
class ResourceHolder;

typedef ResourceHolder<sf::Texture, TextureIDs> TextureHolder;
typedef ResourceHolder<sf::Font, FontIDs> FontHolder;
typedef ResourceHolder<sf::Shader, ShaderIDs> ShaderHolder;
typedef ResourceHolder<sf::SoundBuffer, SoundEffectIDs> SoundBufferHolder;
