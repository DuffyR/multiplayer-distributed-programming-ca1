#include "SettingsState.hpp"
#include "Utility.hpp"
#include "ResourceHolder.hpp"

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

SettingsState::SettingsState(StateStack & stack, Context context)
  : State(stack, context), mGUIContainer()
{
  // Build key binding buttons and labels
  for (std::size_t p = 0; p < 2; ++p) {
    std::string playerID = std::string(" (P") + std::to_string(p + 1) + std::string(")");

    addButtonLabel(static_cast<int>(PlayerAction::MoveUp), p, 0, "MOVE UP" + playerID, context);
    addButtonLabel(static_cast<int>(PlayerAction::MoveDown), p, 1, "MOVE DOWN" + playerID, context);
  }

  updateLabels();

  auto backButton = std::make_shared<GUI::Button>(context);
  backButton->setPosition(220.f, 350.f);
  backButton->setText("BACK");
  backButton->setCallback(std::bind(&SettingsState::requestStackPop, this));

  mGUIContainer.pack(backButton);
}

void SettingsState::draw()
{
  sf::RenderWindow& window = *getContext().window;
  window.setView(window.getDefaultView());

  sf::RectangleShape backgroundShape;
  backgroundShape.setFillColor(sf::Color(155, 188, 15));
  backgroundShape.setSize(window.getView().getSize());

  window.draw(backgroundShape);
  window.draw(mGUIContainer);
}

bool SettingsState::update(sf::Time dt)
{
  return true;
}

bool SettingsState::handleEvent(const sf::Event & event)
{
  int actionCountCasted = static_cast<int>(PlayerAction::Count);
  bool isKeyBinding = false;

  // Iterate through all key binding buttons to see if they are being pressed, waiting for the user to enter a key
  for (std::size_t i = 0; i < 2 * actionCountCasted; ++i)
  {
    if (mBindingButtons[i]->isActive())
    {
      isKeyBinding = true;
      if (event.type == sf::Event::KeyPressed)
      {
        // Player 1
        if (i < actionCountCasted) {
          getContext().keys1->assignKey(static_cast<PlayerAction>(i), event.key.code);
        }
        // Player 2
        else {
          getContext().keys2->assignKey(static_cast<PlayerAction>(i - actionCountCasted), event.key.code);
        }

        mBindingButtons[i]->deactivate();
      }
      break;
    }
  }

  // If pressed button changed key bindings, update labels; otherwise consider other buttons in container
  if (isKeyBinding)
    updateLabels();
  else
    mGUIContainer.handleEvent(event);

  return false;
}

void SettingsState::updateLabels()
{
  for (std::size_t i = 0; i < static_cast<int>(PlayerAction::Count); ++i) {
    auto action = static_cast<PlayerAction>(i);

    // Get keys of both players
    sf::Keyboard::Key key1 = getContext().keys1->getAssignedKey(action);
    sf::Keyboard::Key key2 = getContext().keys2->getAssignedKey(action);

    // Assign both key strings to labels
    mBindingLabels[i]->setText(toString(key1));
    mBindingLabels[i + static_cast<int>(PlayerAction::Count)]->setText(toString(key2));
  }
}

void SettingsState::addButtonLabel(std::size_t index, std::size_t player, std::size_t y, const std::string & text, Context context)
{
  // For x==0, start at index 0, otherwise start at half of array
  index += static_cast<int>(PlayerAction::Count) * player;

  mBindingButtons[index] = std::make_shared<GUI::Button>(context);
  mBindingButtons[index]->setPosition(220.f, 126.f*player + 50.f*y + 100.f);
  mBindingButtons[index]->setText(text);
  mBindingButtons[index]->setToggle(true);

  mBindingLabels[index] = std::make_shared<GUI::Label>("", *context.fonts);
  mBindingLabels[index]->setPosition(445.f, 126.f*player + 50.f*y + 125.f);

  mGUIContainer.pack(mBindingButtons[index]);
  mGUIContainer.pack(mBindingLabels[index]);
}
