#pragma once
const int WORLD_WIDTH = 832;
const int WORLD_HEIGHT = 480;
const int VIEW_WIDTH = 640;
const int VIEW_HEIGHT = 480;
const int UPPER_PADDLE_BOUNDS = 64;
const int LOWER_PADDLE_BOUNDS = 414;

const int MAX_PLAYERS = 2;
const float PADDLE_SPEED = 200.f;
const float BALL_MIN_X_SPEED = 150.f;
const float BALL_START_SPEED = 200.f;
const float BALL_MAX_SPEED = 1000.f;

const float POWERUP_INTERVAL = 15.f;

const float M_PI = 3.1415926535f;