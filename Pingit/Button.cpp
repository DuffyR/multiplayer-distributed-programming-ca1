#include "Button.hpp"
#include "Utility.hpp"
#include "SoundPlayer.hpp"
#include "ResourceHolder.hpp"

#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

GUI::Button::Button(State::Context context)
  : mCallback()
  , mSprite(context.textures->get(TextureIDs::Buttons))
  , mText("", context.fonts->get(FontIDs::Main), 16)
  , mIsToggle(false)
  , mSounds(*context.sounds)
{
  changeTexture(Button::Type::Normal);

  sf::FloatRect bounds = mSprite.getLocalBounds();
  mText.setPosition(bounds.width / 2.f, bounds.height / 2.f);
}

void GUI::Button::setCallback(Callback callback)
{
  mCallback = std::move(callback);
}

void GUI::Button::setText(const std::string & text)
{
  mText.setString(text);
  centerOrigin(mText);
}

void GUI::Button::setToggle(bool flag)
{
  mIsToggle = flag;
}

bool GUI::Button::isSelectable() const
{
  return true;
}

void GUI::Button::select()
{
  Component::select();

  changeTexture(Button::Type::Selected);
}

void GUI::Button::deselect()
{
  Component::deselect();

  changeTexture(Button::Type::Normal);
}

void GUI::Button::activate()
{
  Component::activate();

  // If we are toggle then we should show that the button is pressed and thus "toggled".
  if (mIsToggle)
    changeTexture(Button::Type::Pressed);

  if (mCallback)
    mCallback();

  // If we are not a toggle then deactivate the button since we are just momentarily activated.
  if (!mIsToggle)
    deactivate();

  mSounds.play(SoundEffectIDs::MenuSelection);
}

void GUI::Button::deactivate()
{
  Component::deactivate();

  if (mIsToggle)
  {
    // Reset texture to right one depending on if we are selected or not.
    if (isSelected())
      changeTexture(Button::Type::Selected);
    else
      changeTexture(Button::Type::Normal);
  }
}

void GUI::Button::handleEvent(const sf::Event & event)
{
}

void GUI::Button::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
  states.transform *= getTransform();
  target.draw(mSprite, states);
  target.draw(mText, states);
}

void GUI::Button::changeTexture(Type buttonType)
{
  sf::IntRect textureRect(0, 50 * static_cast<int>(buttonType), 200, 50);
  mSprite.setTextureRect(textureRect);
}
