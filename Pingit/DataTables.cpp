#include "DataTables.hpp"
#include "Constants.hpp"
#include "Particle.hpp"

std::vector<TextureData> initializeTextureData() {
  std::vector<TextureData> data(Data::EntityTypeCount);

  //Paddle - P1
  data[Data::PaddleP1].texture = TextureIDs::Entities;
  data[Data::PaddleP1].textureRect = sf::IntRect(0, 0, 16, 48);
  data[Data::PaddleP1Small].texture = TextureIDs::Entities;
  data[Data::PaddleP1Small].textureRect = sf::IntRect(32, 32, 16, 16);
  data[Data::PaddleP1Big].texture = TextureIDs::Entities;
  data[Data::PaddleP1Big].textureRect = sf::IntRect(48, 0, 16, 64);

  //Paddle - P2
  data[Data::PaddleP2].texture = TextureIDs::Entities;
  data[Data::PaddleP2].textureRect = sf::IntRect(16, 0, 16, 48);
  data[Data::PaddleP2Small].texture = TextureIDs::Entities;
  data[Data::PaddleP2Small].textureRect = sf::IntRect(32, 48, 16, 16);
  data[Data::PaddleP2Big].texture = TextureIDs::Entities;
  data[Data::PaddleP2Big].textureRect = sf::IntRect(64, 0, 16, 64);

  //Ball
  data[Data::Ball].texture = TextureIDs::Ball;
  data[Data::Ball].textureRect = sf::IntRect(0, 0, 32, 32);

  //Wall
  data[Data::Wall].texture = TextureIDs::Entities;
  data[Data::Wall].textureRect = sf::IntRect(32, 16, 16, 16);

  //Powerup
  data[Data::Pickup].texture = TextureIDs::Powerup;
  data[Data::Pickup].textureRect = sf::IntRect(0, 0, 8, 8);

  return data;
}

std::vector<WallPositionData> initializeWallPositionData() {
  std::vector<WallPositionData> data(2);

  data[0].position = sf::Vector2f(0.f, 0.f);
  data[0].size = sf::Vector2f(832.f, 64.f);
  data[0].normal = sf::Vector2f(0.f, 1.f);

  data[1].position = sf::Vector2f(0.f, 414.f);
  data[1].size = sf::Vector2f(832.f, 64.f);
  data[1].normal = sf::Vector2f(0.f, -1.f);

  return data;
}

std::vector<float> initializeSpeedData() {
  std::vector<float> data(Data::EntityTypeCount);

  //Paddle - P1
  data[Data::PaddleP1] = PADDLE_SPEED;

  //Paddle - P2
  data[Data::PaddleP2] = PADDLE_SPEED;

  //Ball
  data[Data::Ball] = BALL_MAX_SPEED;

  //Wall
  data[Data::Wall] = 0.f;

  return data;
}

std::vector<ParticleData> initializeParticleData() {
  std::vector<ParticleData> data(Particle::ParticleCount);

  data[Particle::Dust].color = sf::Color(245, 245, 245);
  data[Particle::Dust].lifetime = sf::seconds(0.8f);

  return data;
}

std::vector<sf::Vector2f> initializeHUDScorePositionData() {
  std::vector<sf::Vector2f> data(MAX_PLAYERS);

  data[0] = sf::Vector2f(256.f, 32.f);
  data[1] = sf::Vector2f(576.f, 32.f);

  return data;
}
