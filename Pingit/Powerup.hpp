#pragma once
//A collectable that changes the size of a paddle
#include "Entity.hpp"
#include "Command.hpp"
#include "ResourceIdentifiers.hpp"
#include "Animation.hpp"

#include <SFML/Graphics/Sprite.hpp>

//Forward declare
class Paddle;

class Powerup : public Entity {
public:
  Powerup(const TextureHolder& textures, int direction, bool makeSmall);

  virtual unsigned int getCategory() const;
  virtual sf::FloatRect	getBoundingRect() const;

  void apply(Paddle& player);

protected:
  virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
  virtual void updateCurrent(sf::Time dt, CommandQueue& commands);

private:
  Animation mAnimatedSprite;
  sf::Time mStartMoveDelay;

  int mTargetDirection;
  bool mMakePaddleSmall; //If false, it's going to make the paddle big instead
};