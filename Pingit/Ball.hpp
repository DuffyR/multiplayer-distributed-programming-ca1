#pragma once
//The object that both players must try and get into the other's goal
#include "Entity.hpp"
#include "Command.hpp"
#include "ResourceIdentifiers.hpp"
#include "Animation.hpp"

#include <SFML/Graphics/Sprite.hpp>

//Forward decleration
class Wall;
class Paddle;

class Ball : public Entity {
public:
  Ball(const TextureHolder& textures, const FontHolder& fonts);

  virtual unsigned int getCategory() const;
  virtual sf::FloatRect	getBoundingRect() const;
  float getMaxSpeed() const;

  void setDraw(bool draw);
  void setIfShouldMove(bool move);

  void playLocalSound(CommandQueue& commands, SoundEffectIDs effect);
  void reboundBallOffWall(Wall& wall);
  void reboundBallOffPaddle(Paddle& paddle);
  void reset();

private:
  virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
  virtual void updateCurrent(sf::Time dt, CommandQueue& commands);

  void startBallMove();

private:
  sf::Sprite mStaticSprite;
  Animation mRotationSprite;
  sf::Time mBallStartMoveDelay; //The ball won't immediatley move when the game starts
  bool mCanMove;
  bool mCanDraw;
};