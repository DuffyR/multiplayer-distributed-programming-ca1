#include "CreditsState.hpp"
#include "Utility.hpp"
#include "ResourceHolder.hpp"

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

namespace {
  const sf::Time Interval(sf::seconds(3.f));
}

CreditsState::CreditsState(StateStack & stack, Context context)
  :State(stack, context)
  , mCountdown(Interval)
{
  //Set up the texts
  std::string texts[] = {"GRAPHICS\nBY\nME (RONAN DUFFY)", "SOUND EFFECTS\nBY\nME (RONAN DUFFY)", "INVERTED COLOR SHADER\nBY\nME (AGAIN)", "TITLE SCREEN MUSIC\n'CHIPTUNE SHOPPING ADVENTURES'\nTEKNOAXE", "GAME MUSIC\n'DIGITAL ETHER'\nTEKNOAXE", "ORIGINAL CODE\nBY\nJOHN LOANE" };

  //Transfer the texts
  for (int i = 0; i < 6; i++) {
    sf::Text thisText(texts[i], context.fonts->get(FontIDs::Main));
    thisText.setFillColor(sf::Color(15, 56, 15));
    centerOrigin(thisText);
    thisText.setPosition(sf::Vector2f(context.window->getSize() / 2u));

    mTexts.push(thisText);
  }
}

void CreditsState::draw() {
  sf::RenderWindow& window = *getContext().window;
  window.setView(window.getDefaultView());

  // Create green-ish background
  sf::RectangleShape backgroundShape;
  backgroundShape.setFillColor(sf::Color(155, 188, 15));
  backgroundShape.setSize(window.getView().getSize());
  window.draw(backgroundShape);

  if (!mTexts.empty())
    window.draw(mTexts.front());
}

bool CreditsState::update(sf::Time dt) {
  mCountdown -= dt;

  if (mCountdown <= sf::Time::Zero) {
    mTexts.pop();
    if (mTexts.empty())
      requestStackPop();
    mCountdown = Interval;
  }

  return false;
}

bool CreditsState::handleEvent(const sf::Event & event) {
  if (mCountdown > sf::seconds(2.f)) { return false; }

  //The player can go to the next screen if they press particular buttons
  //This will skip the SplashState sequence
  sf::Keyboard::Key permittedKeys[3] = { sf::Keyboard::Space, sf::Keyboard::Enter, sf::Keyboard::Escape };

  if (event.type == sf::Event::KeyPressed) {
    sf::Keyboard::Key* found = std::find(std::begin(permittedKeys), std::end(permittedKeys), event.key.code);

    if (found != std::end(permittedKeys))
      mCountdown = sf::Time::Zero;
  }

  return false;
}
