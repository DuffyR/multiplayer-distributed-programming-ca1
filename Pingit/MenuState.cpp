#include "MenuState.hpp"
#include "Button.hpp"
#include "Utility.hpp"
#include "MusicPlayer.hpp"
#include "ResourceHolder.hpp"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>

MenuState::MenuState(StateStack & stack, Context context)
  : State(stack, context), mGUIContainer()
{
  sf::Texture& texture = context.textures->get(TextureIDs::TitleScreen);
  mBackgroundSprite.setTexture(texture);

  addDecorativeBall(sf::Vector2f(263.f, 55.f));
  addDecorativeBall(sf::Vector2f(423.f, 55.f));

  auto playButton = std::make_shared<GUI::Button>(context);
  playButton->setPosition(200, 200);
  playButton->setText("Play");
  playButton->setCallback([this]()
  {
    requestStackPop();
    requestStackPush(StateIDs::Game);
  });

  auto tutorialButton = std::make_shared<GUI::Button>(context);
  tutorialButton->setPosition(200, 250);
  tutorialButton->setText("Help");
  tutorialButton->setCallback([this]()
  {
    requestStackPush(StateIDs::Tutorial);
  });

  auto settingsButton = std::make_shared<GUI::Button>(context);
  settingsButton->setPosition(200, 300);
  settingsButton->setText("Settings");
  settingsButton->setCallback([this]()
  {
    requestStackPush(StateIDs::Settings);
  });

  auto creditsButton = std::make_shared<GUI::Button>(context);
  creditsButton->setPosition(200, 350);
  creditsButton->setText("Credits");
  creditsButton->setCallback([this]()
  {
    requestStackPush(StateIDs::Credits);
  });

  auto exitButton = std::make_shared<GUI::Button>(context);
  exitButton->setPosition(200, 400);
  exitButton->setText("Exit");
  exitButton->setCallback([this]()
  {
    requestStackPop();
  });

  mGUIContainer.pack(playButton);
  mGUIContainer.pack(tutorialButton);
  mGUIContainer.pack(settingsButton);
  mGUIContainer.pack(creditsButton);
  mGUIContainer.pack(exitButton);

  // Play menu theme
  context.music->play(MusicIDs::MenuTheme);
}

void MenuState::draw()
{
  sf::RenderWindow& window = *getContext().window;

  window.setView(window.getDefaultView());

  window.draw(mBackgroundSprite);
  window.draw(mGUIContainer);

  for (auto& ball : mDecorativeBalls)
    window.draw(ball);
}

bool MenuState::update(sf::Time dt)
{
  for (auto& ball : mDecorativeBalls)
    ball.update(dt);

  return true;
}

bool MenuState::handleEvent(const sf::Event & event)
{
  mGUIContainer.handleEvent(event);
  return false;
}

void MenuState::addDecorativeBall(sf::Vector2f position)
{
  Animation ball(getContext().textures->get(TextureIDs::Ball));
  ball.setFrameSize(sf::Vector2i(32, 32));
  ball.setNumFrames(8);
  ball.setDuration(sf::seconds(2));
  ball.setRepeating(true);
  centerOrigin(ball);
  ball.setPosition(position);
  mDecorativeBalls.push_back(ball);
}
